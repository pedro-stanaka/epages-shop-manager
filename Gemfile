# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.5.1'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.2.0'
gem 'pg', '>= 0.18', '< 2.0'
gem 'puma', '~> 3.11'
gem 'redis-rails', '~> 5.0'

gem 'sassc-rails', '~> 1.3'
gem 'uglifier', '>= 1.3.0'
gem 'mini_racer', '~> 0.2'
gem 'coffee-rails', '~> 4.2'
gem 'turbolinks', '~> 5'
gem 'jquery-rails', '~> 4.3'
gem 'bootstrap', '~> 4.1.3'

gem 'figaro', '~> 1.1'
gem 'devise', '~> 4.4'
gem 'faraday_middleware', '~> 0.12.2'
gem 'faraday', '~> 0.15.2'
gem 'epages-rest', '~> 1.0'

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.1.0', require: false

group :development, :test do
  gem 'pry'
  gem 'pry-byebug'
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  gem 'rspec-rails', '~> 3.7'
  gem 'factory_bot_rails', '~> 4.8'
  gem 'faker', '~> 1.9'
  gem 'rubycritic', require: false
end

group :test do
  gem 'shoulda-matchers', '~> 3.0'
  gem 'database_cleaner', '~> 1.5'
  gem 'webmock', '~> 3.4'
  gem 'vcr', '~> 4.0'
  gem 'simplecov', '~> 0.16.1', require: false
end

group :development do
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'rubocop'
  gem 'rubocop-rails_config'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]

# ePages Team 42 Test

## Analytics App for ePages

This application was written in Rails 5 using Ruby 2.5.

## Dependencies

* PostgreSQL (To record user information)
* Redis (for caching requests)
* RSpec 3

## Setup

1. Copy the file `config/application.yml.sample` to `config/application.yml`
2. Configure the environment variables for your database and other pieces of 
infrastructure.
3. Finally run `bin/setup` to setup and migrate the DB.

## Testing

For testing I used RSpec 3 and VCR since the application depends mostly on calls to 
the ePages API.
To run the tests you can run in the terminal:

`bundle exec rspec`

## Deploying

The application deployment is fairly simple in PaaS such as Heroku.
You just need to set the variables and get the redis addon to get the caching working 
properly.

## Missing Improvements

Due to time restriction, I was not able to implement some changes/improvements I 
had planned, namely:

* Using deferred jobs (SideKiq) to trigger pre-cached API calls, this would help the 
analytics part of the application, and make it much more responsible.

* Sync data from the API to the database to allow for easier/cheaper requests for the API.
Since the API has a per hour quota for requests, it would be better if the application 
synced the data with the database and updated the information either via webhooks or doing
scheduled updates. 
 
## Project decisions

In order to try to overcome the problem with the missing improvements I used a read-through 
cache in the most expensive operations (list orders of products and customers of product).
Caching the orders allows the application to avoid hitting the API too much and getting 
over the request quota.

I know that this is not the optimal flow, since we have to load all the orders in memory and that
may cripple the server running the application, but as previously stated I couldn't solve the 
way I think is the right one to do.

I also used an external caching engine to avoid overloading even more the memory on the app server.

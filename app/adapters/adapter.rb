# frozen_string_literal: true

module Adapter
  class EPages
    def self.create_access_token(code, token_url)
      response = http_client(token_url).post '',
          code: code,
          client_id: Rails.configuration.app[:client_id],
          client_secret: Rails.configuration.app[:client_secret]

      unless response.success?
        raise ArgumentError.new('The provided code is not valid.')
      end

      response.body[:access_token]
    end

    private
      def self.http_client(base_url)
        Faraday.new(url: base_url) do |faraday|
          faraday.request :url_encoded

          faraday.response :json,
                           content_type: /\bjson$/,
                           parser_options: { symbolize_names: true }

          faraday.adapter Faraday.default_adapter
        end
      end
  end
end

$(document).on 'change', '#product-analytic-select', ->
  attribute = $(this).val()
  id = $(this).data 'id'

  $.get "/products/#{id}/#{attribute}", (response) ->
    $('.analytics-value').html(response)


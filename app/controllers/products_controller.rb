# frozen_string_literal: true

class ProductsController < ApplicationController
  def index
    page = params[:page] || 1
    @products = shop.products(sort: :name, page: page)
  end

  def show
    @product = shop.product(params[:id])
  end

  def orders
    @orders = Orders::Listing.new(shop).by_product(params[:id])
    @title_prefix = 'Placed'

    render partial: 'products/partials/orders', layout: false
  end

  def orders_unplaced
    @orders = Orders::Listing.new(shop).by_product(params[:id], false)
    @title_prefix = 'Unplaced'

    render partial: 'products/partials/orders', layout: false
  end

  def customers
    @customers = Customers::Listing.new(shop).by_product(params[:id])
    @product = shop.product(params[:id])

    render partial: 'products/partials/customers', layout: false
  end

  def sales
    @sales = shop.sales(product_id: params[:id])

    render partial: 'products/partials/sales', layout: false
  end

  private
    def shop
      @shop ||= Epages::REST::Shop.new(
        current_user.shop_host,
          current_user.shop_name,
          current_user.access_token
      )
    end
end

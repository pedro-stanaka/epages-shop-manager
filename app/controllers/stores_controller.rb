# frozen_string_literal: true

class StoresController < ApplicationController
  skip_before_action :authenticate_user!, only: :connect

  def connect
    session.merge!(account_params)

    redirect_to new_user_registration_path
  end

  private

    def account_params
      params.permit(:code, :api_url, :return_url, :access_token_url, :signature)
    end
end

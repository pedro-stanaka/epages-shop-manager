# frozen_string_literal: true

class Users::RegistrationsController < Devise::RegistrationsController
  # POST /resource
  def create
    unless session.has_key?(:code) &&
        session.has_key?(:access_token_url) &&
        session.has_key?(:api_url)
      handle_error t('errors.messages.no_oauth_data')
      return
    end

    unless Auth::SignatureVerify.new(
      session[:code],
      session[:access_token_url],
      session[:signature]
    ).call
      handle_error t('errors.messages.invalid_signature')
      return
    end

    begin
      access_token = Adapter::EPages.create_access_token(session[:code], session[:access_token_url])

      super do |user|
        user.access_token = access_token
        user.api_url = session[:api_url]
        user.save!
      end
    rescue ArgumentError => exception
      handle_error exception.message
      return
    end
  end

  private
    def handle_error(message)
      flash.alert = message
      redirect_to new_user_session_url
    end
end

# frozen_string_literal: true

class User < ApplicationRecord
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  validates :email, presence: true, uniqueness: true
  validates :password_confirmation, presence: true, if: Proc.new { |u| u.password.present? }
  validates_presence_of :api_url, :access_token

  def shop_host
    api_uri.host
  end

  def shop_name
    api_uri.path.gsub('/rs/shops/', '')
  end

  def api_uri
    URI(api_url)
  end
end

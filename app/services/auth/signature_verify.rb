# frozen_string_literal: true

module Auth
  class SignatureVerify
    attr_reader :auth_code, :token_url, :original_signature

    def initialize(auth_code, token_url, original_signature)
      @auth_code = auth_code
      @token_url = token_url
      @original_signature = original_signature
    end

    def call
      client_secret = Rails.configuration.app[:client_secret]
      binary_digest = OpenSSL::HMAC.digest('sha256', client_secret, data)

      calculated_signature = Base64.strict_encode64(binary_digest)

      ActiveSupport::SecurityUtils.secure_compare(calculated_signature, original_signature)
    end

    def data
      auth_code + ':' + token_url
    end
  end
end

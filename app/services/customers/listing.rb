# frozen_string_literal: true

module Customers
  class Listing
    attr_reader :shop

    def initialize(shop)
      @shop = shop
    end

    def by_product(product_id)
      orders = Orders::Listing.new(shop).by_product(product_id)
      customer_ids = orders.map { |o| o.customer_id }

      fetch_customers(customer_ids, product_id)
    end

    private
      def fetch_customers(customer_ids, product_id)
        Rails.cache.fetch("#{shop.name}/products/#{product_id}/customers") do
          customer_ids.map do |id|
            shop.customer(id)
          end
        end
      end
  end
end

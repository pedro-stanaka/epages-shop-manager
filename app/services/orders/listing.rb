# frozen_string_literal: true

module Orders
  class Listing
    MAX_RESULTS = 100

    attr_reader :shop

    def initialize(shop)
      @shop = shop
    end

    def by_product(product_id, placed = true)
      orders.select do |order|
        order_has_product(order, product_id) && order.paid_on.nil? != placed
      end
    end

    private

      def order_has_product(order, product_id)
        !order.line_item_container.product_line_items.select do |line|
          line.product_id == product_id
        end.empty?
      end

      def orders
        @orders ||= fetch_ord
      end

      def fetch_ord
        Rails.cache.fetch("#{shop.shop_name}/orders", expires_in: 3.hours) do
          raw_orders = fetch_raw_orders
          orders = []

          raw_orders.each do |o|
            order = shop.order(o)

            orders << order
          end

          orders
        end
      end

      def fetch_raw_orders
        Rails.cache.fetch("#{shop.shop_name}/orders_raw", expires_in: 3.hours) do
          fetch_api_orders
        end
      end

      def fetch_api_orders(page = 1, orders = [])
        response = fetch_orders_paginated(page)
        orders = orders + response.items
        total_pages = (response.results / response.results_per_page.to_f).ceil

        fetch_api_orders(page + 1, orders) if page < total_pages

        orders
      end

      def fetch_orders_paginated(page)
        shop.orders(results_per_page: Listing::MAX_RESULTS, page: page)
      end
  end
end

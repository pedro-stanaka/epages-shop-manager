# frozen_string_literal: true

required_keys = %w(PROD_DATABASE_URL DEVISE_SECRET_KEY APP_CLIENT_ID APP_CLIENT_SECRET)

Figaro.require_keys(required_keys)

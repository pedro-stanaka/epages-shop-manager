# frozen_string_literal: true

Rails.application.routes.draw do
  root to: 'products#index'
  devise_for :users, controllers: {
      registrations: 'users/registrations'
  }

  resources :stores, only: [] do
    get '/connect', on: :collection, to: 'stores#connect'
  end

  resources :products, only: [:index, :show] do
    member do
      get '/orders', to: 'products#orders'
      get '/orders_unplaced', to: 'products#orders_unplaced'
      get '/customers', to: 'products#customers'
      get '/sales', to: 'products#sales'
    end
  end
end

# frozen_string_literal: true

class AddApiUrlShopNameAccessTokenToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :api_url, :string, null: false
    add_column :users, :access_token, :string, null: false
  end
end

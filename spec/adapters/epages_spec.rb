# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Adapter::EPages do
  let(:api_url) { 'https://devshop.epages.com/rs/shops/tiny-mouse-0686/token' }

  context 'with valid auth code' do
    let(:code) { 'd42SxNwDNcNlUXWhC8ekF1jABXnb1B75' }
    it 'returns a valid access token' do
      VCR.use_cassette('epages_connect') do
        access_token = Adapter::EPages.create_access_token(code, api_url)

        expect(access_token).to be_a(String)
      end
    end
  end

  context 'with invalid auth code' do
    let(:code) { 'invalid_code' }

    it 'raises argument error' do
      VCR.use_cassette('epages_connect_invalid') do
        expect do
          Adapter::EPages.create_access_token(code, api_url)
        end.to raise_error(ArgumentError, 'The provided code is not valid.')
      end
    end
  end
end

# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    email { Faker::Internet.email }
    password 'test123'
    password_confirmation 'test123'
    api_url 'https://tiny-mouse-0686.devshop.epages.com/rs/shops/tiny-mouse-0686'
    access_token 'NkKaCU8Ldg7Z5y2H7D1ojzNDbCRdszIS'
  end
end

# frozen_string_literal: true

require 'rails_helper'

RSpec.describe User, type: :model do
  it { is_expected.to validate_presence_of(:email) }
  it { is_expected.to validate_presence_of(:api_url) }
  it { is_expected.to validate_presence_of(:access_token) }

  context 'instance methods' do
    let(:user) { build_stubbed(:user) }
    let(:uri) { URI(user.api_url) }

    describe '#api_uri' do
      subject { user.api_uri }

      it 'returns the URI object built from the api_url' do
        is_expected.to eq uri
      end
    end

    describe '#shop_host' do
      subject { user.shop_host }

      it 'returns the host from #api_uri' do
        is_expected.to eq uri.host
      end
    end

    describe '#shop_name' do
      before do
        user.api_url = 'https://some-shop-123.epages.com/rs/shops/some-shop-123'
      end
      subject { user.shop_name }

      it 'returns the path with default prefix removed' do
        is_expected.to eq 'some-shop-123'
      end
    end
  end

  context 'when password is present' do
    subject { User.new(password: 'Test123') }

    it { is_expected.to validate_presence_of(:password_confirmation) }
  end

  context 'when password is not present' do
    subject { User.new }

    it { is_expected.not_to validate_presence_of(:password_confirmation) }
  end
end

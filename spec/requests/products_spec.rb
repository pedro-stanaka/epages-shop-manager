# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Products', type: :request do
  describe 'GET /' do
    context 'when not authenticated' do
      it 'redirects to login' do
        get root_path
        expect(response).to redirect_to new_user_session_url
      end
    end

    context 'when authenticated' do
      it 'shows the product list' do
        sign_in(create(:user))

        VCR.use_cassette('product_index_list') do
          get root_path

          # Header
          expect(response.body).to include('Store Products')

          # Products
          expect(response.body).to include('Navy Boots')
        end
      end
    end
  end

  describe 'GET /products/:id' do
    context 'when not authenticated' do
      it 'redirects to login' do
        get '/products/some-id'

        expect(response).to redirect_to new_user_session_url
      end
    end

    context 'when authenticated' do
      it 'shows the product details' do
        sign_in(create(:user))

        VCR.use_cassette('product_show') do
          get '/products/5B5A647B-755A-7713-846D-0A0C05B4F20C'

          expect(response.body).to include('Vintage Leather Armchair')
          expect(response.body).to include('With its soft cushions and wooden legs')
          expect(response.body).to include('167.98')
        end
      end
    end
  end

  describe 'GET /products/:id/orders' do
    it 'shows the orders of a product' do
      sign_in(create(:user))

      VCR.use_cassette('orders_listing') do
        get '/products/5B5A647C-14E5-CB9C-8025-0A0C05B4F283/orders'

        expect(response.body).to match(/Order number:.*1002/)
      end
    end
  end

  describe 'GET /products/:id/orders_unplaced' do
    it 'shows the orders of a product' do
      sign_in(create(:user))

      VCR.use_cassette('orders_listing') do
        get '/products/5B5A647C-14E5-CB9C-8025-0A0C05B4F283/orders_unplaced'

        expect(response.body).to match(/Order number:.*1007/)
      end
    end
  end

  describe 'GET /products/:id/customers' do
    it 'shows the buyers of a product' do
      sign_in(create(:user))

      VCR.use_cassette('customer_listing') do
        get '/products/5B5A647C-14E5-CB9C-8025-0A0C05B4F283/customers'

        expect(response.body).to match(/Navy Boots \(.*1006\)/)
      end
    end
  end

  describe 'GET /products/:id/sales' do
    it 'shows the sale volume for the product' do
      sign_in(create(:user))

      VCR.use_cassette('sales_amount') do
        get '/products/5B5A647C-14E5-CB9C-8025-0A0C05B4F283/sales'

        expect(response.code).to eq '200'

        expect(response.body).to match(/Currency/)
        expect(response.body).to match(/EUR/)
        expect(response.body).to match(/Gross Revenue/)
        expect(response.body).to match(/162.48/)
      end
    end
  end
end

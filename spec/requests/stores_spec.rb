# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'StoresController', type: :request do
  describe 'GET /stores/connect' do
    context 'when connecting a new account' do
      let(:params) do
        {
            code: 'exp_code',
            api_url: 'foo',
            return_url: 'return_foo',
            access_token_url: 'tok_valid'
        }
      end

      it 'creates new session and redirect to the sign up page' do
        get '/stores/connect', params: params

        expect(session[:code]).to eq params[:code]
        expect(session[:api_url]).to eq params[:api_url]
        expect(session[:return_url]).to eq params[:return_url]
        expect(session[:access_token_url]).to eq params[:access_token_url]

        expect(response).to redirect_to new_user_registration_path
      end
    end
  end
end

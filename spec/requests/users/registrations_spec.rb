# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'User Registrations Controller', type: :request do
  describe 'POST /users' do
    let(:user_params) do
      {
          user: {
              email: 'usertest@gmail.com',
              password: 'test123',
              password_confirmation: 'test123'
          }
      }
    end

    let!(:oauth_data) do
      data = {
          code: 'eLoJC7BI5PpEUlGMSII8AMf99pKsONe6',
          access_token_url: 'https://devshop.epages.com/rs/shops/tiny-mouse-0686/token',
          api_url: 'https://tiny-mouse-0686.devshop.epages.com/rs/shops/tiny-mouse-0686'
      }

      data[:signature] = get_valid_signature_for(data[:code], data[:access_token_url])

      data
    end

    context 'when no information on session' do
      it 'redirects to login_url with flash' do
        post '/users', params: user_params

        is_expected.to redirect_to(new_user_session_url)
        expect(flash[:alert]).to eq I18n.t('errors.messages.no_oauth_data')
      end
    end

    context 'when invalid signature' do
      it 'redirects to login_url with proper flash' do
        oauth_data[:signature] = 'invalid_sign'

        # Setup
        get '/stores/connect', params: oauth_data
        is_expected.to redirect_to new_user_registration_path

        # Post with session set
        post '/users', params: user_params
        is_expected.to redirect_to(new_user_session_url)
        expect(flash[:alert]).to eq I18n.t('errors.messages.invalid_signature')
      end
    end

    context 'when invalid auth code' do
      it 'throws bad request telling the user to create account again' do
        # Setup
        get '/stores/connect', params: oauth_data
        is_expected.to redirect_to new_user_registration_path

        # Post with correct session, but invalid code
        VCR.use_cassette('epages_connect_invalid') do
          post '/users', params: user_params

          expect(flash[:alert]).to match(/The provided code is not valid/)
          is_expected.to redirect_to(new_user_session_url)
        end
      end
    end

    context 'when valid request' do
      it 'creates the user and redirects him to /' do
        # Setup
        get '/stores/connect', params: oauth_data
        is_expected.to redirect_to new_user_registration_path

        # Post with correct session
        VCR.use_cassette('epages_connect') do
          post '/users', params: user_params
          is_expected.to redirect_to(root_path)
        end
      end
    end
  end
end

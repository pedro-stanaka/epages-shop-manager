# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Auth::SignatureVerify do
  describe '#call' do
    let(:code) { '88888888' }
    let(:url) { 'https://devshop.epages.com/rs/shops/sample-shop/token' }
    let(:client_secret) { 'supersecret123' }
    let(:signature) { get_valid_signature_for(code, url) }

    subject { Auth::SignatureVerify.new(code, url, signature).call }

    context 'with valid signature' do
      before do
        Rails.configuration.app[:client_secret] = client_secret
      end

      it { is_expected.to be_truthy }
    end

    context 'with invalid signature' do
      let(:signature) { 'invalid_sign' }

      it { is_expected.to be_falsey }
    end
  end
end

# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Customers::Listing do
  describe '#by_product' do
    let(:user) { create(:user) }
    let(:shop) do
      Epages::REST::Shop.new(
        user.shop_host,
          user.shop_name,
          user.access_token
      )
    end
    let(:product_id) { '5B5A647C-14E5-CB9C-8025-0A0C05B4F283' }
    subject { described_class.new(shop).by_product(product_id) }

    it 'lists all the customers who bought a product' do
      VCR.use_cassette('customer_listing') do
        expect(subject.size).to eq 1
        expect(subject.map { |c| c.customer_number }).to include('1001')
      end
    end
  end
end

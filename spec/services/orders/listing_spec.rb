# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Orders::Listing do
  describe '#placed_by_product' do
    let(:user) { create(:user) }
    let(:shop) do
      Epages::REST::Shop.new(
        user.shop_host,
          user.shop_name,
          user.access_token
      )
    end
    let(:product_id) { '5B5A647C-14E5-CB9C-8025-0A0C05B4F283' }

    context 'when filtering placed orders' do
      subject { described_class.new(shop).by_product(product_id) }

      it 'returns only placed orders where the product appeared' do
        VCR.use_cassette('orders_listing') do
          expect(subject.length).to eq 1

          expect(subject.map { |o| o.order_number }).to include('1002')
        end
      end
    end

    context 'when not filtering by placed orders' do
      subject { described_class.new(shop).by_product(product_id, false) }

      it 'returns only placed orders where the product appeared' do
        VCR.use_cassette('orders_listing') do
          expect(subject.length).to eq 1

          expect(subject.map { |o| o.order_number }).to include('1007')
        end
      end
    end
  end
end

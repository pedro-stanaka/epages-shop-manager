# frozen_string_literal: true

module HmacSignatureHelper
  def get_valid_signature_for(code, url)
    secret = Rails.configuration.app[:client_secret]
    digest = OpenSSL::HMAC.digest('sha256', secret, code + ':' + url)
    Base64.strict_encode64(digest)
  end
end
